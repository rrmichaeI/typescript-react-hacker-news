import React from "react";

export type colorTheme = "light" | "dark";

const ThemeContext = React.createContext<colorTheme>("light");

export default ThemeContext;
export const ThemeConsumer = ThemeContext.Consumer;
export const ThemeProvider = ThemeContext.Provider;
