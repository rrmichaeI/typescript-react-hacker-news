import React from "react";
import queryString from "query-string";

import { formatDate } from "../utils/helpers";
import { fetchUser, fetchPosts, User, Post } from "../utils/api";
import Loading from "./Loading";
import PostsList from "./PostsList";

interface UserState {
	user: User | null;
	loadingUser: boolean;
	posts: Post[] | null;
	loadingPosts: boolean;
	error: Error | null;
}

type UserReducerActions =
	| { type: "fetch" }
	| { type: "user"; user: User }
	| { type: "posts"; posts: Post[] }
	| { type: "error"; message: Error };

function userReducer(state: UserState, action: UserReducerActions): UserState {
	if (action.type === "fetch") {
		return {
			...state,
			loadingUser: true,
			loadingPosts: true,
		};
	} else if (action.type === "user") {
		return {
			...state,
			user: action.user,
			loadingUser: false,
		};
	} else if (action.type === "posts") {
		return {
			...state,
			posts: action.posts,
			loadingPosts: false,
			error: null,
		};
	} else if (action.type === "error") {
		return {
			...state,
			error: action.message,
			loadingPosts: false,
			loadingUser: false,
		};
	} else {
		throw new Error(`That action type is not supported.`);
	}
}

export default function UserComponent({
	location,
}: {
	location: { search: string };
}) {
	const { id } = queryString.parse(location.search) as { id: string };

	const [state, dispatch] = React.useReducer(userReducer, {
		user: null,
		loadingUser: true,
		posts: null,
		loadingPosts: true,
		error: null,
	});

	React.useEffect(() => {
		dispatch({ type: "fetch" });

		fetchUser(id)
			.then((user) => {
				dispatch({ type: "user", user });
				return fetchPosts(user.submitted.slice(0, 30));
			})
			.then((posts) => dispatch({ type: "posts", posts }))
			.catch(({ message }) => dispatch({ type: "error", message }));
	}, [id]);

	const { user, posts, loadingUser, loadingPosts, error } = state;

	if (error) {
		return <p className="center-text error">{error}</p>;
	}

	return (
		<React.Fragment>
			{loadingUser === true ? (
				<Loading text="Fetching User" />
			) : (
				user && (
					<React.Fragment>
						<h1 className="header">{user.id}</h1>
						<div className="meta-info-light">
							<span>
								joined <b>{formatDate(user.created)}</b>
							</span>
							<span>
								has <b>{user.karma.toLocaleString()}</b> karma
							</span>
						</div>
						{user.about && (
							<p
								dangerouslySetInnerHTML={{ __html: user.about }}
							/>
						)}
					</React.Fragment>
				)
			)}
			{loadingPosts === true ? (
				loadingUser === false && <Loading text="Fetching posts" />
			) : (
				<React.Fragment>
					<h2>Posts</h2>
					{posts && <PostsList posts={posts} />}
				</React.Fragment>
			)}
		</React.Fragment>
	);
}
