import React from "react";
import PropTypes from "prop-types";

import { fetchMainPosts, Post, PostCategory } from "../utils/api";
import Loading from "./Loading";
import PostsList from "./PostsList";

interface PostsState {
	posts: Post[] | null;
	loading: boolean;
	error: Error | null;
}

type PostsReducerActions =
	| { type: "fetch" }
	| { type: "success"; posts: Post[] }
	| { type: "error"; message: Error };

function postsReducer(
	state: PostsState,
	action: PostsReducerActions
): PostsState {
	if (action.type === "fetch") {
		return {
			posts: null,
			error: null,
			loading: true,
		};
	} else if (action.type === "success") {
		return {
			posts: action.posts,
			error: null,
			loading: false,
		};
	} else if (action.type === "error") {
		return {
			posts: state.posts,
			error: action.message,
			loading: false,
		};
	} else {
		throw new Error(`That action type is not supported.`);
	}
}

export default function Posts({ category }: { category: PostCategory }) {
	const [state, dispatch] = React.useReducer(postsReducer, {
		posts: null,
		error: null,
		loading: true,
	});

	React.useEffect(() => {
		dispatch({ type: "fetch" });

		fetchMainPosts(category)
			.then((posts) => dispatch({ type: "success", posts }))
			.catch(({ message }: { message: Error }) =>
				dispatch({ type: "error", message })
			);
	}, [category]);

	if (state.loading === true) {
		return <Loading />;
	}

	if (state.error) {
		return <p className="center-text error">{state.error}</p>;
	}

	return state.posts && <PostsList posts={state.posts} />;
}

Posts.propTypes = {
	type: PropTypes.oneOf(["top", "new"]),
};
