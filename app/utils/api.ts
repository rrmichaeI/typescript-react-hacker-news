const api = `https://hacker-news.firebaseio.com/v0`;
const json = ".json?print=pretty";

export type PostType = "story" | "comment";
export type PostCategory = "top" | "new";

export interface Post {
	id: string;
	deleted?: boolean;
	type: PostType;
	by: string;
	time: number;
	text?: string;
	dead?: boolean;
	parent?: number;
	poll?: number;
	kids?: string[];
	url?: string;
	score?: number;
	title?: string;
	parts?: number[];
	descendants?: number;
}

export interface User {
	id: string;
	created: number;
	karma: number;
	about?: string;
	submitted?: string[];
	delay?: number;
}

function removeDead(posts: Post[]) {
	return posts.filter(Boolean).filter(({ dead }) => dead !== true);
}

function removeDeleted(posts: Post[]) {
	return posts.filter(({ deleted }) => deleted !== true);
}

function onlyComments(posts: Post[]) {
	return posts.filter(({ type }) => type === "comment");
}

function onlyPosts(posts: Post[]) {
	return posts.filter(({ type }) => type === "story");
}

export function fetchItem(id: string): Promise<Post> {
	return fetch(`${api}/item/${id}${json}`).then((res) => res.json());
}

export function fetchComments(ids: string[]): Promise<Post[]> {
	return Promise.all(ids.map(fetchItem)).then((comments) =>
		removeDeleted(onlyComments(removeDead(comments)))
	);
}

export function fetchMainPosts(category: PostCategory) {
	return fetch(`${api}/${category}stories${json}`)
		.then((res) => res.json())
		.then((ids: string[]) => {
			if (!ids) {
				throw new Error(
					`There was an error fetching the ${category} posts.`
				);
			}

			return ids.slice(0, 50);
		})
		.then((ids) => Promise.all(ids.map(fetchItem)))
		.then((posts) => removeDeleted(onlyPosts(removeDead(posts))));
}

export function fetchUser(id: string) {
	return fetch(`${api}/user/${id}${json}`).then((res) => res.json());
}

export function fetchPosts(ids: string[]): Promise<Post[]> {
	return Promise.all(ids.map(fetchItem)).then((posts) =>
		removeDeleted(onlyPosts(removeDead(posts)))
	);
}
